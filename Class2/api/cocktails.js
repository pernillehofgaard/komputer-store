export const getAllCocktails = () =>{
    return fetch("https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=Gin")
    .then(response => response.json())
    .then(data => data.drinks)
    //Promise
}