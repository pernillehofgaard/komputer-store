import { getAllCocktails } from './api/cocktails.js';

function App(){
    this.elCoctailList = document.getElementById("cocktails");
    this.drinks = [];

}

App.prototype.getCocktails = async function() {
    
    try{
        this.drinks = await getAllCocktails();
    }catch(e){

    }finally{
        this.render();
    }
    
}

App.prototype.render = function() {
    console.log(this);

    this.drinks.forEach(drink =>{
        const elDrink = document.createElement("li");
        elDrink.className = "drink";

        const elDrinkName = document.createElement('h4');
        elDrinkName.innerText = drink.strDrink;
        elDrink.appendChild(elDrinkName);

        const img = new Image();
        img.width = 150;
        img.onload = fuction() {
            elDrink.appendChild(img);
        }
        img.src = drink.strDrinkThumb;
        this.elCoctailList.appendChild(elDrink);
    });
    
}

const app = new App();
app.getCocktails();