1 - The Bank
the banke shows a Bank balance in kr. This is the amount available to you to buy a laptop.

Get A Loan
The get Loan button will attempt to get a loan from the bank. When the get loan is clicked, it must show a "Prompt" popup box that allows you to enter
an amount.

Constraints:
1 - you cannot get a loan more than double of your bank balance
2 - you cannot get more than one loan before buying a computer

2 - Work
Pay
the pay amount in kr. Should show how much money you have earned by "working". This money is NOT part of your bank balance.

Bank button
The bank button must transfer the money from your pay balance, to your bank balance.
Remember to reset your pay one you transfer.

Work button
The work button must increase your pay balance by 100 kr on each click.


3 - Laptops
Use a select box to show at least 4 different laptops. Each laptop must have a unique name and price.
You should find a way to store a name, price, description, feature list and image link for each laptop. The
feature list of the selected laptop must be displayed here. Changing a laptop should update the user
interface with the information for that selected laptop.
The large box at the bottom or the Info section is where the image, name and description as well as the
price of the laptop must be displayed.
Buy Now button
The buy now button is the final action of your website. This button will attempt to “Buy” a laptop and
validate whether the bank balance is sufficient to purchase the selected laptop.
If you do not have enough money in the “Bank”, a message must be shown that you cannot afford the
laptop.
When you have sufficient “Money” in the account, the amount must be deducted from the bank and
you must receive a message that you are now the owner of the new laptop!