let count = 0;
let loanCount = 0;

const ComputerList = [
    { name: 'HP', price: 5000, description: "Old hp computer. It ran the sims3 and Command & Conquer pretty well though", pic: "Images/hp.jpg" },
    { name: 'MSI', price: 6599, description: "MSI laptop can run Skyrim with 60 fps on low graphic settings", pic: "Images/MSI.jpg"},
    { name: 'Alien Ware',price: 24000,description: "Alienware can run all your favorite games with 60 fps on ultra high graphic settings, <br/>however the cpu requires a lot of air and the lack of air might sufficate you.", pic: "Images/alienware.jpg" },
    { name: 'Apple', price: 9999, description: "Macbook wich you pay way too much for", pic: "Images/mac.jpg"}  
];

// we dont want to show a borken img icon. Hide the pic until user selects pc
document.getElementById("computerImg").style.display ="none";
// we also dont want to show a buy btn befor the user has selected a pc.
document.getElementById("btnBuy").style.display = "none";

function work(){
    count++;
    let earned = document.getElementById("moneyEarned");
    //intEarned = Number(earned);
    earned.innerHTML = count * 100;
}

function bankMoney(){
    const earned = document.getElementById("moneyEarned");
    const account = document.getElementById("accountBalance");

    let currentEarned = Number(earned.innerHTML);
    let currentAccount = Number(account.innerHTML);

    if(currentEarned > 0){

        account.innerHTML = (currentEarned + currentAccount);
        earned.innerHTML = 0;
        console.log("currentEarned: " + currentEarned);
        console.log("currentAccount: " + currentAccount);
        count = 0;

    }

}

function bankLoan(){
    const loanAmount = prompt("Enter amount you would like to loan:");

    let account = document.getElementById("accountBalance");
    let currentAccount = Number(account.innerHTML);
    console.log("currentAccount: " +  currentAccount);

    if(loanCount > 0){
        alert("You already have a loan")
    }else if(loanAmount > (currentAccount*2)){
        alert("You can only loan twice as much as your account balance")
    }else{
        loanCount++;
        account.innerHTML = Number(loanAmount) + Number(currentAccount);
        console.log("Account.innerHTML: " + account.innerHTML);
    }  
}

function selectValue(){
    document.getElementById("computerImg").style.display ="block";
    document.getElementById("btnBuy").style.display = "block";

    let value = document.getElementById("select").value;
    console.log(value);

    const pcName = document.getElementById("computerName");
    const pcPrice = document.getElementById("computerPrice");
    const pcDescription = document.getElementById("computerDescription");
    

    pcName.innerHTML = ComputerList[value].name;
    pcPrice.innerHTML = ComputerList[value].price;
    pcDescription.innerHTML = ComputerList[value].description;
    document.getElementById("computerImg").src = ComputerList[value].pic;
}

function buyComputer(){
    const value = document.getElementById("select").value;
    console.log("you want to buy " + ComputerList[value].name);

    const yourAccountBalance = document.getElementById("accountBalance");
    let balance = yourAccountBalance.innerHTML;

    console.log("Account balance: " + balance);
    const pcPrice = ComputerList[value].price;
    if(pcPrice > balance){
        alert("You dont have enough money. Work some more!");
    }else{
        alert("You are now the proude owner of an " + ComputerList[value].name + " computer!");
        yourAccountBalance.innerHTML = Number(balance) - Number(pcPrice);
        console.log("Balance after purchase: " + balance)
        loanCount = 0;
    }

}
